-- --------------------------------------------------------
-- Хост:                         localhost
-- Версия сервера:               PostgreSQL 14.2, compiled by Visual C++ build 1914, 64-bit
-- Операционная система:         
-- HeidiSQL Версия:              12.0.0.6468
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES  */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Дамп данных таблицы public.tm_project: 8 rows
/*!40000 ALTER TABLE "tm_project" DISABLE KEYS */;
INSERT IGNORE INTO "tm_project" ("id", "created", "user_id", "name", "description", "status") VALUES
	('e45a3aff-1966-4718-a9bc-077354c46140', '2022-07-22 14:26:21.135', 'c99db38d-d526-4367-94dd-837ae4b5aa39', 'Project_01', 'Desc_01_user 1', 'IN_PROGRESS'),
	('7b706b55-1752-4945-9feb-98dc20b8f7cc', '2022-07-22 14:26:21.26', 'c99db38d-d526-4367-94dd-837ae4b5aa39', 'Project_02', 'Desc_02_user 1', 'NOT_STARTED'),
	('ea2d522b-1fd8-42c8-bee2-9987dee33683', '2022-07-22 14:26:21.405', 'c99db38d-d526-4367-94dd-837ae4b5aa39', 'Project_03', 'Desc_03_user 1', 'IN_PROGRESS'),
	('b3b714b0-aa01-4f51-9fce-0c5de7e6a2a1', '2022-07-22 14:26:21.541', 'c99db38d-d526-4367-94dd-837ae4b5aa39', 'Project_04', 'Desc_04_user 1', 'COMPLETED'),
	('7fd13c02-b83e-4a83-a091-172b688f1656', '2022-07-22 14:26:21.656', 'bcfb593e-0b8b-4e5e-a7e7-89ecf9597e89', 'Project_01', 'Desc_01_user 2', 'IN_PROGRESS'),
	('45342554-11e5-46b3-9f4f-be515db14d67', '2022-07-22 14:26:21.873', 'bcfb593e-0b8b-4e5e-a7e7-89ecf9597e89', 'Project_02', 'Desc_02_user 2', 'NOT_STARTED'),
	('c98ff991-0b16-4744-ae3c-92261567e394', '2022-07-22 14:26:22.082', '5d120b0e-1086-4600-9d3e-1476b5a94cfa', 'Project_01', 'Desc_01_user 3', 'IN_PROGRESS'),
	('352124a2-f989-43ff-8cb2-948726d0c88c', '2022-07-22 14:26:22.193', '5d120b0e-1086-4600-9d3e-1476b5a94cfa', 'Project_02', 'Desc_02_user 3', 'COMPLETED');
/*!40000 ALTER TABLE "tm_project" ENABLE KEYS */;

-- Дамп данных таблицы public.tm_session: -1 rows
/*!40000 ALTER TABLE "tm_session" DISABLE KEYS */;
/*!40000 ALTER TABLE "tm_session" ENABLE KEYS */;

-- Дамп данных таблицы public.tm_task: -1 rows
/*!40000 ALTER TABLE "tm_task" DISABLE KEYS */;
INSERT IGNORE INTO "tm_task" ("id", "created", "user_id", "name", "description", "status", "project_id") VALUES
	('b50de671-59c4-4db4-8243-ae2bf0eb2c46', '2022-07-31 14:33:30.664', 'c99db38d-d526-4367-94dd-837ae4b5aa39', 'task 1', 'test', 'NOT_STARTED', NULL),
	('a23260f3-803e-44fd-8d08-6e182a7ae91f', '2022-07-22 14:26:23.503', 'bcfb593e-0b8b-4e5e-a7e7-89ecf9597e89', 'Task_01', 'Desc task 1 user 2', 'NOT_STARTED', '45342554-11e5-46b3-9f4f-be515db14d67'),
	('eab69479-b325-4235-919b-2fa9aeed23b0', '2022-07-22 14:26:23.618', 'bcfb593e-0b8b-4e5e-a7e7-89ecf9597e89', 'Task_02', 'Desc task 2 user 2', 'NOT_STARTED', NULL),
	('d750bc4a-24cc-4d37-ab91-7d47cf75bd90', '2022-07-22 14:26:23.732', '5d120b0e-1086-4600-9d3e-1476b5a94cfa', 'Task_01', 'Desc task 1 user 3', 'NOT_STARTED', '352124a2-f989-43ff-8cb2-948726d0c88c'),
	('064b987c-d74e-4c19-a024-884f88de5dc3', '2022-07-22 14:26:23.844', '5d120b0e-1086-4600-9d3e-1476b5a94cfa', 'Task_01', 'Desc task 2 user 3', 'NOT_STARTED', NULL),
	('257a3e24-cb99-4f59-adea-eddecda30a33', '2022-07-31 14:35:21.795', 'c99db38d-d526-4367-94dd-837ae4b5aa39', 'new task 3', 'test 3', 'IN_PROGRESS', 'e45a3aff-1966-4718-a9bc-077354c46140'),
	('514ef090-5bbf-43c7-a731-fe6f869fbd92', '2022-07-31 14:34:37.653', 'c99db38d-d526-4367-94dd-837ae4b5aa39', 'task 2', 'test', 'NOT_STARTED', NULL);
/*!40000 ALTER TABLE "tm_task" ENABLE KEYS */;

-- Дамп данных таблицы public.tm_user: -1 rows
/*!40000 ALTER TABLE "tm_user" DISABLE KEYS */;
INSERT IGNORE INTO "tm_user" ("id", "created", "login", "password", "email", "first_name", "last_name", "middle_name", "role", "locked") VALUES
	('bcfb593e-0b8b-4e5e-a7e7-89ecf9597e89', '2022-07-22 14:26:20', 'user', '98ec4d73e5d7ef05ce531448d187c8de', 'user@user.ru', NULL, NULL, NULL, 'USUAL', 'false'),
	('5d120b0e-1086-4600-9d3e-1476b5a94cfa', '2022-07-22 14:26:21', 'admin', 'f0bdfdaf314d2c393a22a0bc7e8fbeb1', NULL, NULL, NULL, NULL, 'ADMIN', 'false'),
	('c99db38d-d526-4367-94dd-837ae4b5aa39', '2022-07-22 14:26:20', 'test', '85dd1f0c499504865fe62ee1ebac6040', 'test@test.ru', 'firstName', 'lastName', 'middleName', 'USUAL', 'false');
/*!40000 ALTER TABLE "tm_user" ENABLE KEYS */;

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
