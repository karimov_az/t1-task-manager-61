package ru.t1.karimov.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.karimov.tm.api.service.dto.IDtoService;
import ru.t1.karimov.tm.dto.model.AbstractDtoModel;
import ru.t1.karimov.tm.exception.entity.EntityNotFoundException;
import ru.t1.karimov.tm.exception.field.IdEmptyException;
import ru.t1.karimov.tm.exception.field.IndexIncorrectException;
import ru.t1.karimov.tm.repository.dto.IDtoRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public abstract class AbstractDtoService<M extends AbstractDtoModel> implements IDtoService<M> {

    @NotNull
    protected static final String ERROR_INDEX_OUT_OF_BOUNDS = "Error! Index оut of bounds...";

    @NotNull
    @Autowired
    protected IDtoRepository<M> repository;

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final M result = repository.save(model);
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> add(@NotNull final Collection<M> models) {
        @NotNull final List<M> newModels = new ArrayList<>(repository.saveAll(models));
        return newModels;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        return repository.findAll(PageRequest.of(index,1))
                .stream().findFirst().orElse(null);
    }

    @NotNull
    @Override
    public Long getSize() {
        return repository.count();
    }

    @Override
    @Transactional
    public void removeAll() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.delete(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException(ERROR_INDEX_OUT_OF_BOUNDS);
        @NotNull final M model = Optional.ofNullable(findOneByIndex(index)).orElseThrow(EntityNotFoundException::new);
        repository.delete(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        repository.deleteAll();
        @NotNull final List<M> newModels = new ArrayList<>(repository.saveAll(models));
        return newModels;
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        repository.save(model);
    }

}
