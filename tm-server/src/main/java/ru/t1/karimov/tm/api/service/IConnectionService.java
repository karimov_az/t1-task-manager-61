package ru.t1.karimov.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull
    Liquibase getLiquibase();

}
