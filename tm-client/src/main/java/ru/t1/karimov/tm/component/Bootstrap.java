package ru.t1.karimov.tm.component;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.IListener;
import ru.t1.karimov.tm.api.endpoint.*;
import ru.t1.karimov.tm.api.service.ILoggerService;
import ru.t1.karimov.tm.api.service.IPropertyService;
import ru.t1.karimov.tm.api.service.IServiceLocator;
import ru.t1.karimov.tm.api.service.ITokenService;
import ru.t1.karimov.tm.event.ConsoleEvent;
import ru.t1.karimov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.karimov.tm.exception.system.CommandNotSupportedException;
import ru.t1.karimov.tm.listener.AbstractListener;
import ru.t1.karimov.tm.util.SystemUtil;
import ru.t1.karimov.tm.util.TerminalUtil;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Getter
@Setter
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String TASK_MANAGER_PID = "task-manager.pid";

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    @Autowired
    private AbstractListener[] abstractListeners;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @SneakyThrows
    private void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(TASK_MANAGER_PID), pid.getBytes());
        @NotNull final File file = new File(TASK_MANAGER_PID);
        file.deleteOnExit();
    }

    private void processArguments(@Nullable final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.exit(0);
        } catch (@NotNull final Exception e) {
            System.out.println(e.getMessage());
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    private void processArgument(@Nullable final String arg) throws Exception {
        @Nullable final IListener listener = getListenerByArgument(arg);
        if (listener == null) throw new ArgumentNotSupportedException(arg);
        publisher.publishEvent(new ConsoleEvent(listener.getName()));
    }

    @Nullable
    private AbstractListener getListenerByArgument(@Nullable final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        for (@Nullable final AbstractListener listener : abstractListeners) {
            if (listener != null && argument.equals(listener.getArgument())) return listener;
        }
        return null;
    }

    public void processCommand(@Nullable final String command) throws Exception {
        @Nullable final IListener abstractListener = getListenerByCommand(command);
        if (abstractListener == null) throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(abstractListener.getName()));
    }

    @Nullable
    private AbstractListener getListenerByCommand(@Nullable final String command) {
        if (command == null || command.isEmpty()) return null;
        for (@Nullable final AbstractListener listener : abstractListeners) {
            if (listener != null && command.equals(listener.getName())) return listener;
        }
        return null;
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    public void start(@Nullable final String[] args) {
        processArguments(args);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

}
